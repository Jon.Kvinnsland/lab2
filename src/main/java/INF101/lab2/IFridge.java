package INF101.lab2;

import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;

public interface IFridge {

	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	int nItemsInFridge();

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	int totalSize();

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	boolean placeIn(FridgeItem item);


	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	void takeOut(FridgeItem item);

	/**
	 * Remove all items from the fridge
	 */
	void emptyFridge();

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	List<FridgeItem> removeExpiredFood();

}

class Fridge implements IFridge {
	int maxItems = 20;
	ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
	
	
	@Override
	public int nItemsInFridge() {
		return items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return maxItems;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge() < 20) {
			items.add(item);
			return true;
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		if(items.contains(item)) {
			items.remove(item);
		} else {
			throw new NoSuchElementException();
		}
		}

	@Override
	public void emptyFridge() {
		items.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredItems = new ArrayList<>();
		for (int i = 0; i < nItemsInFridge(); i++) {
			if (items.get(i).hasExpired()) {
				expiredItems.add(items.get(i));
				takeOut(items.get(i));
				i -= 1;
			}
		}
		return expiredItems;
	}
}

